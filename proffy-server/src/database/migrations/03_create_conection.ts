import Knex from 'knex';

export const up = async (knex: Knex) => {
  return knex.schema.createTable('conection', table => {
    table.increments('id').primary();

    table.integer('week_day').notNullable();
    table.integer('from').notNullable();
    table.integer('to').notNullable();

    table
      .integer('user_id')
      .notNullable()
      .references('id')
      .inTable('users')
      .onUpdate('CASCADE');

    table.timestamp('created_at').defaultTo('now()').notNullable();
  });
};

export const down = async (knex: Knex) => knex.schema.dropTable('conection');
