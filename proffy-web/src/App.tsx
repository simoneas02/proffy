import React from 'react';

import Routes from './routes';

import './assets/style/global.css';

const App = () => <Routes />;

export default App;
