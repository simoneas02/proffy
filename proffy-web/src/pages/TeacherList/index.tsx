import React from 'react';

import PageHeader from '../../components/PageHeader';
import TeacherItem from '../../components/TeacherItem';

const TeacherList = () => (
  <div>
    <PageHeader title="Estes são os proffys disponíveis!">
      <form>
        <div>
          <label htmlFor="subject">Matérias</label>
          <input type="text" name="subject" id="subject" />
        </div>

        <div>
          <label htmlFor="weekDay">Dia da Semana</label>
          <input type="text" name="weekDay" id="weekDay" />
        </div>

        <div>
          <label htmlFor="time">Horário</label>
          <input type="text" name="time" id="time" />
        </div>
      </form>
    </PageHeader>
    <main>
      <TeacherItem />
      <TeacherItem />
      <TeacherItem />
      <TeacherItem />
      <TeacherItem />
    </main>
  </div>
);

export default TeacherList;
