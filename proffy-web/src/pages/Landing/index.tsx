import React from 'react';

import logo from '../../assets/images/logo.svg';
import landing from '../../assets/images/landing.svg';
import study from '../../assets/images/icons/study.svg';
import teach from '../../assets/images/icons/give-classes.svg';
import heart from '../../assets/images/icons/purple-heart.svg';

import './styles.css';
import { Link } from 'react-router-dom';

const Landing = () => (
  <div className="landing">
    <div className="container">
      <div className="logo-container">
        <img src={logo} alt="Proffy" />
        <h2>Sua plataforma de estudos</h2>
      </div>

      <img src={landing} alt="Plataforma de estudos" />
      <div className="buttons-container">
        <Link to="/study" className="study">
          <span>Estudar</span>
          <img src={study} alt="Estudar" />
        </Link>

        <Link to="/teach" className="teach">
          <span>Ensinar</span>
          <img src={teach} alt="Ensinar" />
        </Link>

        <span className="total-conections">
          Total de 200 conexões já realizadas{' '}
          <img src={heart} alt="Coração roxo" />
        </span>
      </div>
    </div>
  </div>
);

export default Landing;
