import React from 'react';

import whatsapp from '../../assets/images/icons/whatsapp.svg';

const TeacherItem = () => (
  <article>
    <header>
      <img
        src="https://avatars2.githubusercontent.com/u/7841344?s=460&u=7160c0fcc3c851f0b521d151bbe535300165a6c6&v=4"
        alt="Simone Amorim"
      />

      <div>
        <h3>Simone</h3>
        <h4>CSS Lover</h4>
      </div>
    </header>
    <p>
      Mother, WWCode Leader, front-end developer, and CSS evangelist. I'm
      passionate about running and bike riding
    </p>

    <footer>
      <p>Preço / hora</p>
      <span>50,00</span>

      <button>
        <img src={whatsapp} alt="whatsapp" />
        Entrar em contato
      </button>
    </footer>
  </article>
);

export default TeacherItem;
