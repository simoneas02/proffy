import React from 'react';
import { Link } from 'react-router-dom';

import back from '../../assets/images/icons/back.svg';
import logo from '../../assets/images/logo.svg';

interface PageHeaderProps {
  title: String;
}

const PageHeader: React.FC<PageHeaderProps> = ({ title, children }) => (
  <header>
    <div>
      <Link to="/">
        <img src={back} alt="Voltar" />
      </Link>

      <img src={logo} alt="Proffy" />
    </div>

    <div>
      <h3>{title}</h3>
      {children}
    </div>
  </header>
);

export default PageHeader;
