import React from 'react';
import { BrowserRouter, Route } from 'react-router-dom';

import Landing from './pages/Landing';
import TeachetList from './pages/TeacherList';
import TeachetForm from './pages/TeacherForm';

const Routes = () => (
  <BrowserRouter>
    <Route path="/" exact component={Landing} />
    <Route path="/study" component={TeachetList} />
    <Route path="/teach" component={TeachetForm} />
  </BrowserRouter>
);

export default Routes;
